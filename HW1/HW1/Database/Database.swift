//
// Created by Виталя on 03/11/2018.
// Copyright (c) 2018 Dreams of Pines. All rights reserved.
//

import Foundation

protocol Database {

    func write(user: User, handler: @escaping (String?)->())
    func readProfile(handler: @escaping (Profile?)->())

}
