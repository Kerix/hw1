//
// Created by Виталя on 03/11/2018.
// Copyright (c) 2018 Dreams of Pines. All rights reserved.
//

import Foundation
import CoreData

class StoragePersistence {

    let nameContainer = "Model"

    var persistence: NSPersistentContainer?
    
    static var entityProfile = "Profile"
    static var instance: StoragePersistence?
    
    static func create() -> StoragePersistence{
        if let instance = instance{
            return instance
        }
        instance = StoragePersistence()
        return instance!
    }


    init(){
        persistence = NSPersistentContainer(name: nameContainer)
        persistence?.loadPersistentStores(){(_,error) in
            if let error = error{
                assertionFailure(error.localizedDescription);
            }
        }
    }
}


extension StoragePersistence: Database{
    func write(user: User, handler: @escaping (String?)->()) {
        persistence?.performBackgroundTask(){ [weak self] context in
            self?.readProfile(){ profile in
                var err: String? = nil
                do{
                    let profile = NSEntityDescription.insertNewObject(forEntityName: StoragePersistence.entityProfile,
                                                                        into: context) as? Profile
                    profile?.name = user.name
                    profile?.abouSelf = user.aboutSelf
                    profile?.imageUri = user.uri
                    try context.save()

                }catch{
                    err = "error"
                }
                handler(err)
            }
        }
    }

    func readProfile(handler: @escaping (Profile?)->()){
        persistence?.performBackgroundTask { (context) in
            let fetchRequest = NSFetchRequest<Profile>(entityName: StoragePersistence.entityProfile)
            let profiles = try? context.fetch(fetchRequest)
            guard let profile = profiles?.last else {
                handler(nil)
                return
            }
            handler(profile)
        }
    }
}

