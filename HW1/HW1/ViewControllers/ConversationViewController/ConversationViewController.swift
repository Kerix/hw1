//
//  ConversationViewController.swift
//  HW1
//
//  Created by Виталя on 03.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit

class ConversationViewController: UIViewController {

    @IBOutlet private  weak var tableView: UITableView!
    
    private let sections: [String] = [ "Online", "History" ]
    private var chatsInfo: [Int : [(peer: Peer, message: Message)]] = [:]

    private var networkApi: NetworkApi?
    private let identifire = String(describing: ConversationListCell.self)
    private let profileIdentifire = String(describing: ViewController.self)
    private let chatIdentifire = String(describing: ChatViewController.self)
    private let sizeRow: CGFloat = 92
    private let titleChat = "Tinkoff Chat"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingTableView()
        networkApi = NetworkApi.create()
        networkApi!.delegate = self
        networkApi!.online = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateListUser()
    }

    private func updateListUser(){
        var chats: [Peer: (connect: Bool, online: Bool, messages: [(peer: Peer, message: Message)])]
            = networkApi!.peersList
        chatsInfo = [:]
        for peer in chats.keys{
            let section = chats[peer]!.online ? 0 : 1
            let messages = chats[peer]!.messages
            let message = messages.count == 0 ? Message(empty: true) : messages[messages.count-1].message
            if chatsInfo[section] == nil{
                chatsInfo[section] = []
            }
            chatsInfo[section]!.append((peer: peer ,message: message))
        }
        tableView.reloadData()
    }


    private func settingTableView(){
        tableView.register(UINib(nibName: identifire, bundle: nil), forCellReuseIdentifier: identifire)
        tableView.estimatedRowHeight = sizeRow
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    @IBAction func openProfile(_ sender: UIBarButtonItem){
        if let vc = UIStoryboard(name: profileIdentifire, bundle: nil).instantiateInitialViewController(){
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    private func showInvitationAlert(peer: Peer, handler: @escaping (Bool)->()){
        let alert = UIAlertController(title: "Приглашение", message: "\(peer.name), приглашает початиться", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {_ in handler(false)}))
        alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: {_ in handler(true)}))
        self.present(alert,animated: true, completion: nil)
    }
}


extension ConversationViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = chatsInfo[section] else{
            return 0
        }
        return section.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifire, for: indexPath) as? ConversationListCell else{
            return UITableViewCell()
        }

        let peer = chatsInfo[indexPath.section]![indexPath.row].peer
        let message = chatsInfo[indexPath.section]![indexPath.row].message

        cell.set(peer: peer, message: message)

        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    
}

extension ConversationViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return sizeRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let selectedPeer = chatsInfo[indexPath.section]![indexPath.row].peer
        if networkApi!.peersList[selectedPeer]!.connect {
            if let vc = UIStoryboard(name: chatIdentifire, bundle: nil).instantiateInitialViewController() as? ChatViewController{
                vc.user = chatsInfo[indexPath.section]![indexPath.row].peer
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            //showAllertInvite
            networkApi!.sendInvite(selectedPeer)
        }
    }
}

extension ConversationViewController: CommunicationServiceDelegate{
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer) {
        self.updateListUser()
    }

    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer) {
        self.updateListUser()
    }

    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error) {
        //showAlertError
    }

    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: @escaping (Bool) -> Void) {
        showInvitationAlert(peer: peer, handler: {answer in invintationClosure(answer)})
    }

    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error) {
        //showAlertError
    }

    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message, from peer: Peer) {
        updateListUser()
    }
    
    func communicationService(_ communicationService: ICommunicationService, peer: Peer, connect: Bool) {
        //showAlertCancelOrACcept
    }
    
}

