//
//  ViewController.swift
//  HW1
//
//  Created by Виталя on 20.09.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit


//  Послание для проверяющего насчет 7 задания:
//
//  Была идея сохранять изменения через Json формат, поэтому появился дополнительный
//  класс FileSaver, но к сожалению эта идея не было до конца реализована из-за недостачи времени
//  надеюсь это не критично
//
//  Также было замечено, что из-за изображения притормаживает эмулятор, возможно из-за того, что я
//  его кодирую в base64 для сохранения в файл
//
//

class ViewController: UIViewController{
    
    
    static var unknowed = "Не задано"
    
    
    @IBOutlet private weak var aboutTextView: UITextView!
    @IBOutlet private weak var nameUserTextView: UITextView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var changImageButton: UIButton!
    @IBOutlet private weak var changeDescriptionButton: UIButton!
    
    @IBOutlet private weak var stackSaveButtons: UIStackView!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet private weak var closeBtn: UIBarButtonItem!
    
    private let gcdDataManager = GCDDataManager()
    private let operationDataManager = OperationDataManager()
    
    private var state: State?
    
    private let changedUser: User = User()
    private let oldUser: User = User()
    
    private var wasChangeImage = false
    private var wasChangeName = false
    private var wasChangeAboutSelf = false
    private let storage = StoragePersistence.create()
    var photoURL: URL?
    
    enum State{
        case Editing
        case NotEditing
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settingViews()
        readProfileFromBD()
//        loadData(dataManage: operationDataManager)
    }
    
    private func settingUsers(user: User?){
        oldUser.name = (user == nil ? nameUserTextView.text :  user?.name)!
        oldUser.aboutSelf = (user == nil ? aboutTextView.text : user?.aboutSelf)!
        oldUser.uri = user == nil ? nil : user?.uri
        changedUser.name = oldUser.name
        changedUser.aboutSelf = oldUser.aboutSelf
        changedUser.uri = oldUser.uri
    }
    
    @IBAction func closeProfile(_ sender: UIBarButtonItem){
        if(state != State.Editing && !(wasChangeImage || wasChangeAboutSelf || wasChangeName)){
            self.dismiss(animated: true, completion: nil)
        }else{
            showWarningAlert(title: "Warning", message: "You have unsaved data"){[weak self] in
                self?.wasChangeAboutSelf = false
                self?.wasChangeName = false
                self?.wasChangeImage = false
                self?.state = State.NotEditing
                self?.closeProfile((self?.closeBtn)!)
            }
        }
    }
    
    @IBAction func saveData(sender: UIButton){
        settingCurrentUser()
        saveProfileToBD()
//        saveData(user: changedUser, dataManager: isLeftBtn ? gcdDataManager : operationDataManager)
    }
    
    private func readProfileFromBD(){
        storage.readProfile(){ [weak self] profile in
            guard let profile = profile else {
                DispatchQueue.main.async { [weak self] in
                    self?.settingUsers(user: nil)
                    self?.updateScreen(user: nil)    // Поправить потом (это для себя)
                }
                return
            }
            let name = profile.name ?? ViewController.unknowed
            let aboutSelf = profile.abouSelf ?? ViewController.unknowed
            self?.photoURL = profile.imageUri
            let user = User(name: name, aboutSelf: aboutSelf, uri: self?.photoURL)
            DispatchQueue.main.async { [weak self] in
                self?.settingUsers(user: user)
                self?.updateScreen(user: user)
            }
            
        }
    }
    
    private func saveProfileToBD(){
        loadingIndicator.isHidden = false
        blockViewWhileSaving(block: true)
        storage.write(user: changedUser){[weak self] err in
            if err != nil{
                self?.showAlert(title: "Error", message: "Не удалось сохранить данные"){ [weak self] in
                    self?.saveProfileToBD()
                }
                self?.updateScreen(user: nil)
            }else{
                self?.showAlert(title: "Success", message: nil, handler: nil)
                self?.readProfileFromBD()
            }
        }
    }
    
    private func settingCurrentUser(){
        if wasChangeImage  {changedUser.uri = photoURL }
        if wasChangeName   {changedUser.name = nameUserTextView.text}
        if wasChangeAboutSelf {changedUser.aboutSelf = aboutTextView.text}
    }
    
    @IBAction func changeInformationClick(_ sender: UIButton){
        hideViewsForEditing(hide: false)
        updateStateButton()
    }

    private func  saveData(user: User, dataManager: DataManager){
        loadingIndicator.isHidden = false
        blockViewWhileSaving(block: true)
        dataManager.save(user: user){ [weak self] success in
            if success {
                self?.showAlert(title: "Success", message: nil, handler: nil)
                self?.loadData(dataManage: dataManager)
            }else{
                self?.showAlert(title: "Error", message: "Не удалось сохранить данные"){ [weak self] in
                    self?.saveData(user: user, dataManager: dataManager)
                }
                self?.updateScreen(user: nil)
            }
        }
    }
    
    private func blockViewWhileSaving(block: Bool){
        changImageButton.isEnabled = !block
        aboutTextView.isEditable = !block
        nameUserTextView.isEditable = !block
        closeBtn.isEnabled = !block
        stackSaveButtons.isUserInteractionEnabled = !block
    }
    
    private func loadData(dataManage: DataManager){
        DispatchQueue.main.async { [weak self] in
            dataManage.load(){ [weak self]  user  in
                if let user = user {
                    self?.settingUsers(user: user)
                    self?.updateScreen(user: user)
                }else{
                    self?.updateScreen(user: nil)
                }
            }
        }
    }
    
    private func updateScreen(user: User?){
            wasChangeAboutSelf = false
            wasChangeName = false
            wasChangeImage = false
            if let user = user {
                updateAvatar(url: user.uri)
                nameUserTextView.text = user.name
                aboutTextView.text = user.aboutSelf
            }
            loadingIndicator.isHidden = true
            blockViewWhileSaving(block: false)
            endChangeInformation()
    }
    
    private func endChangeInformation(){
        hideViewsForEditing(hide: true)
    }
    
    private func updateAvatar(url: URL?){
        guard let url = url else{
            return
        }
        let data = try? Data(contentsOf: url)
        guard data != nil else{
            return
        }
        avatarImageView.image = UIImage(data: data!)
    }
    
    private func hideViewsForEditing(hide: Bool){
        nameUserTextView.isEditable = !hide
        aboutTextView.isEditable = !hide
        changeDescriptionButton.isHidden = !hide
        stackSaveButtons.isHidden = hide
        changImageButton.isHidden = hide
        state = hide ? State.NotEditing : State.Editing
        nameUserTextView.backgroundColor = hide ? UIColor.greyColorWithAlpha : UIColor.white
        aboutTextView.backgroundColor = hide ? UIColor.greyColorWithAlpha : UIColor.white
    }
    
    private func settingViews(){
        changImageButton.layer.cornerRadius = 0.5 * changImageButton.bounds.size.width
        avatarImageView.layer.cornerRadius = changImageButton.layer.cornerRadius
        changeDescriptionButton.layer.borderWidth = 2;
        changeDescriptionButton.layer.borderColor = (UIColor.black).cgColor
        changeDescriptionButton.layer.cornerRadius = 10
        nameUserTextView.textContainer.maximumNumberOfLines = 1
        nameUserTextView.textContainer.lineBreakMode = .byTruncatingTail
        nameUserTextView.delegate = self
        loadingIndicator.isHidden = true
        aboutTextView.delegate = self
    }
    
    private func updateStateButton(){
        stackSaveButtons.isUserInteractionEnabled = wasChangeName ||
            wasChangeImage || wasChangeAboutSelf
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    @IBAction func changeImageBtnClick(_ sender: UIButton) {
        showChoosePhotoAlert()
    }
    
    private func showChoosePhotoAlert(){
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self

        let sheet = UIAlertController(title: "Choose photo", message: nil, preferredStyle: .actionSheet)
        
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        sheet.addAction(UIAlertAction(title: "From gallery", style: .default){ (action) in
            self.showImagePickerController(imagePicker: imagePickerController, sourceType: .photoLibrary, error: "Gallery not available")
        })
        
        sheet.addAction(UIAlertAction(title: "From camera", style: .default){ (action) in
            self.showImagePickerController(imagePicker: imagePickerController, sourceType: .camera, error: "Camera not available")
        })
        present(sheet, animated: true, completion: nil)
    }
    
    private func showImagePickerController(imagePicker: UIImagePickerController, sourceType:UIImagePickerControllerSourceType, error:String){
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            imagePicker.sourceType = sourceType
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            self.showAlert(title: "Error", message: error, handler: nil)
        }
    }
    

    private func showAlert(title: String, message: String?, handler: (()->())?){
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            if handler != nil {
                alert.addAction(UIAlertAction(title: "Repeat", style: .default){ action in handler!()})
            }
            self?.present(alert, animated: true, completion: nil)
        }
    }

    private func showWarningAlert(title: String, message: String?, handler: @escaping ()->()){
        DispatchQueue.main.async { [weak self] in
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Continue", style: .default){ action in handler()})
            self?.present(alert,animated: true, completion: nil)
        }
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        wasChangeImage = true
        updateStateButton()
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            avatarImageView.image = image
            let imgUrl = info[UIImagePickerControllerImageURL] as? URL
            let imgName = imgUrl?.lastPathComponent
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
            let localPath = documentDirectory?.appending(imgName!)
            let data = UIImagePNGRepresentation(image)! as NSData
            data.write(toFile: localPath!, atomically: true)
            photoURL = URL.init(fileURLWithPath: localPath!)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension ViewController: UITextViewDelegate{
    
    func textViewDidChange(_ textView: UITextView) {
        let isNameTextView = textView == nameUserTextView
        
        if isNameTextView{
            wasChangeName = !oldUser.name.elementsEqual(textView.text)
        }else{
            wasChangeAboutSelf = !oldUser.aboutSelf.elementsEqual(textView.text)
        }
        updateStateButton()
    }
}

extension UIColor{
    static let greyColorWithAlpha: UIColor = UIColor(red: 235/255, green: 235/255, blue: 243/255, alpha: 0.4)
}

