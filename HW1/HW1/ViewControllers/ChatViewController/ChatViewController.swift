//
//  ChatViewController.swift
//  HW1
//
//  Created by Виталя on 05.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    
    var user: Peer?
    
    private var mess: [(peer: Peer, message: Message)]?
    
    private let inMessCell = "InMessageCell"
    private let outMessCell = "OutMessageCell"
    
    @IBOutlet private weak var messageWriterView: MessageWriterView!
    @IBOutlet weak var tableView: UITableView!
    
    private let networkApi = NetworkApi.create()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        networkApi.delegate = self
        createArrayMessage()
        settingNavigationBar()
        settingTableView()
        settingMessageWriter()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let user = user {
            addInfoInNavigationBar(user: user)
        }
    }

    private func settingMessageWriter(){
        messageWriterView.sender = self
        messageWriterView.dropShadow(scale: true)
    }
    
    private func settingTableView(){
        tableView.register(UINib(nibName: inMessCell, bundle: nil), forCellReuseIdentifier: inMessCell)
        tableView.register(UINib(nibName: outMessCell, bundle: nil), forCellReuseIdentifier: outMessCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 34
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func updateScreen(){
        createArrayMessage()
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    private func createArrayMessage(){
        mess = networkApi.peersList[user!]?.messages
    }
    
    private func addInfoInNavigationBar(user: Peer){
        navigationItem.title = user.name
    }
    
    private func settingNavigationBar(){
        navigationItem.backBarButtonItem?.action = #selector(backPressed)
    }

    @IBAction private func backPressed(){
        navigationController?.popViewController(animated: true)
    }
        
}

extension ChatViewController: MessageWriterDelegate{

    func sendButtonClick(text: String) {
        let message = Message(identifier: RandomUtils.generateMessageId(), text: text, type: "TextMessage")
        if let user = user {
            networkApi.send(message, to: user)
        }
        updateScreen()
    }
    
}


extension ChatViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let mess = mess else  {
            return 0
        }
        return mess.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if mess?[indexPath.row].peer != networkApi.mainPeer{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: outMessCell , for: indexPath) as? MessageCell else{
                return UITableViewCell()
            }
            cell.setup(text: mess![indexPath.row].message.text)
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: inMessCell , for: indexPath) as? MessageCell else{
                return UITableViewCell()
            }
            cell.setup(text: mess![indexPath.row].message.text)
            return cell
        }
    }

    
}


extension ChatViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ChatViewController: CommunicationServiceDelegate{
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer) {
        if peer == user{
            messageWriterView.isUserInteractionEnabled = true
        }
        updateScreen()
    }
    
    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer) {
        if peer == user{
            messageWriterView.isUserInteractionEnabled = false
        }
        updateScreen()
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error) {
        //
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer, invintationClosure: @escaping (Bool) -> Void) {
        //
    }
    
    func communicationService(_ communicationService: ICommunicationService, didNotStartAdvertisingForPeers error: Error) {
        //
    }
    
    func communicationService(_ communicationService: ICommunicationService, peer: Peer, connect: Bool) {
        //
    }
    
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message, from peer: Peer) {
        updateScreen()
    }
    
    
    
}










