//
//  RandomUtils.swift
//  HW1
//
//  Created by Виталя on 04.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation
import UIKit

struct RandomUtils{
    
    private static let colorArray: [UIColor] = [UIColor.orange, UIColor.brown, UIColor.blue, UIColor.darkGray, UIColor.yellow, UIColor.green, UIColor.gray]
    
    private static let nameArray: [String] = ["Виталий", "Игорь","Михаил", "Андрей", "Ирина", "Ольга","Антон","Светлана","Николай","Владислав"]
    
    private static let familyArray: [String] = ["Пискунов", "Моногошева","Прохоров", "Иванов", "Проскурин", "Медведев","Путин","Шорохов","Шумилов","Крупнов"]
    
    static func random(count: Int) -> Int{
        return Int(arc4random_uniform(UInt32(count)))
    }
    
    static func boolRandom() -> Bool{
        return random(count: 2) == 0
    }
    
    static func colorRandom() -> UIColor{
        return colorArray[random(count: colorArray.count)]
    }
    
    static func nameRandom() -> String{
        return "\(nameArray[random(count: nameArray.count)]) \(familyArray[random(count: familyArray.count)])"
    }
    
    static func dateRandom() -> Date{
        if(boolRandom()){
            return Date()
        }
        return Date(timeIntervalSince1970: TimeInterval(random(count: 1254320)))
    }

    static func generateMessageId() -> String {
        let string = "\(arc4random_uniform(UINT32_MAX))+\(Date.timeIntervalSinceReferenceDate)+\(arc4random_uniform(UINT32_MAX))".data(using: .utf8)?.base64EncodedString()
        return string!
    }

}
