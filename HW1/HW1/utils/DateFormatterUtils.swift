//
//  DateFormatterUtils.swift
//  HW1
//
//  Created by Виталя on 04.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

class DateFormatterUtils{
        
    static func stringForChatFromDate(date : Date?)->String?{
        if let date = date{
            let format = sameDate(date: date) ? "HH:mm" : "dd MMM"
            return stringFromDate(format: format, date: date)
        }
        return nil
    }
    
    static func sameDate(date:Date)->Bool{
        let curDate = Date()
        return sameComponentCalendar(date: date, curDate: curDate, toGranularity: .day) &&
               sameComponentCalendar(date: date, curDate: curDate, toGranularity: .month) &&
               sameComponentCalendar(date: date, curDate: curDate, toGranularity: .year)
    }
    
    static func sameComponentCalendar(date: Date, curDate: Date, toGranularity: Calendar.Component)->Bool{
        return Calendar.current.compare(date, to: curDate, toGranularity: toGranularity).rawValue == 0
    }
    
    static func stringFromDate(format : String, date : Date?)->String?{
        if let date = date{
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "ru_RU")
            dateFormatter.dateFormat = format
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
}
