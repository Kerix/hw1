//
//  StateApp.swift
//  HW1
//
//  Created by Виталя on 20.09.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation
struct StateApp{
    enum App {
        case NotRunning
        case Active
        case InActive
        case Background
        case Suspend
    }
}
