//
//  ConversationCellConfiguration.swift
//  HW1
//
//  Created by Виталя on 04.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

protocol ConversationCellConfiguration: class {
    var name : String? {get set}
    var message : String? {get set}
    var date : Date? {get set}
    var online : Bool {get set}
    var hasUnreadMessage : Bool {get set}
}
