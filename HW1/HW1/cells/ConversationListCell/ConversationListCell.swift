//
//  ConversationListCell.swift
//  HW1
//
//  Created by Виталя on 03.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit


class ConversationListCell: UITableViewCell, ConversationCellConfiguration {
   
    var name : String? {
        willSet {
            nameLabel.text = newValue
        }
    }
    var message : String? {
        willSet {
            messageLabel.text = newValue ?? "No messages yet"
            messageLabel.font = newValue == nil ? UIFont.italicSystemFont(ofSize: messageLabel.font.pointSize) : UIFont.systemFont(ofSize: messageLabel.font.pointSize, weight: .regular )
            emptyMess = newValue == nil
        }
    }
    var date : Date? {
        willSet{
            dateLabel.text = DateFormatterUtils.stringForChatFromDate(date: newValue)
        }
    }
    var online : Bool = false {
        willSet{
            view.backgroundColor = newValue ? UIColor.yellow.withAlphaComponent(0.3) : UIColor.white.withAlphaComponent(1)
        }
    }
    var hasUnreadMessage : Bool = false {
        willSet{
            if(!emptyMess){
                messageLabel.font = newValue ? UIFont.systemFont(ofSize: messageLabel.font.pointSize, weight: .heavy ) : UIFont.systemFont(ofSize: messageLabel.font.pointSize)
            }
            messageLabel.textColor = newValue || emptyMess ? UIColor.black : UIColor.lightGray
        }
    }

    
    @IBOutlet private weak var view: UIView!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var conversationImageView: UIImageView!
    @IBOutlet private weak var messageLabel: UILabel!
    
    private var emptyMess = false

    func set(peer:Peer, message: Message){
        if(message.empty){
            set(name: peer.name,
                    message: nil,
                    date: Date(),
                    online: true,
                    hasUnreadMessage: false)
        }else{
            set(name: peer.name,
                message: message.text,
                date: message.date,
                online: true,
                hasUnreadMessage: false)
        }
        self.conversationImageView.backgroundColor = RandomUtils.colorRandom()
    }

    func set(chat: ShortChatInfo){
        set(name: chat.user?.name,
            message: chat.message,
            date: chat.date,
            online: (chat.user?.online)!,
            hasUnreadMessage: chat.hasUnreadMessage
        )
        self.conversationImageView.backgroundColor = chat.user?.image
    }
    
    func set(name: String?, message: String?, date: Date?, online: Bool, hasUnreadMessage: Bool){
        self.name = name
        self.message = message
        self.date = date
        self.online = online
        self.hasUnreadMessage = hasUnreadMessage
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        conversationImageView.layer.cornerRadius = 0.5 * conversationImageView.bounds.size.width
    }
    
}
