//
//  OutMessageCell.swift
//  HW1
//
//  Created by Виталя on 05.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell, MessageCellConfiguration {

    @IBOutlet private weak var backView: UIView!
    @IBOutlet private weak var textMessageLabel: UILabel!
    
     var textMessage: String?{
        willSet{
            textMessageLabel.text = newValue
        }
    }
    
    func setup(text: String){
        self.textMessage = text
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backView.layer.cornerRadius = 20
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
}



