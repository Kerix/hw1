//
//  ShortChatInfo.swift
//  HW1
//
//  Created by Виталя on 04.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

struct ShortChatInfo{
    var message: String?
    var date: Date?
    var hasUnreadMessage: Bool = false
    var user: User?
}
