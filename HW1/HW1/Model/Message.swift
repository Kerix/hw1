//
//  Message.swift
//  HW1
//
//  Created by Виталя on 05.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

struct Message{
    enum `Type` : String {
        case textMessage = "TextMessage"
    }
    let identifier: String
    let text: String
    let type: Type
    let date: Date
    var empty: Bool = false

    init(identifier: String, text: String, type: String){
        self.identifier = identifier
        self.text = text
        self.type = Message.getType(evenType: type)
        self.date = Date()
    }
    
    init(empty: Bool){
        self.init(identifier: "", text: "", type: "")
        self.empty = true
    }

    init?(from data: Data){
        do {
            let json = try JSONSerialization.jsonObject(with: data) as? [String: String]
            guard let evenType = json?["evenType"] else{
                return nil
            }
            guard let messageId = json?["messageId"] else{
                return nil
            }
            guard let text = json?["text"] else{
                return nil
            }
            self.init(identifier: messageId, text: text, type: evenType)
        }catch{
            return nil
        }
    }
    

    private static func getType(evenType: String)->Type{
        switch evenType {
            case Type.textMessage.rawValue:
                return Type.textMessage
        default:
            return Type.textMessage
        }
    }
    
    func toJSONData() -> Data?{
        var json : [String: String] = [:]
        json["evenType"] = Type.textMessage.rawValue
        json["messageId"] = identifier
        json["text"] = text
        do{
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return data
        }catch{}
        return nil
    }

}
