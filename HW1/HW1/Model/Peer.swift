//
// Created by Виталя on 27.10.2018.
// Copyright (c) 2018 Dreams of Pines. All rights reserved.
//

import Foundation

struct Peer: Hashable{
    let identifier: String
    let name: String

    var hashValue: Int{
        return identifier.hashValue
    }

    init(identifier: String, name: String){
        self.identifier = identifier
        self.name = name
    }

    static func ==(lhs: Peer, rhs: Peer) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}
