//
//  User.swift
//  HW1
//
//  Created by Виталя on 04.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation
import UIKit

class User: NSObject, NSCoding{

    var name: String
    var aboutSelf: String
    var online: Bool = false
    var image: UIColor?
    var imageI: UIImage?
    var uri: URL?
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("User")
    
    
    struct PropertyKeys{
        static let name = "name"
        static let aboutSelf = "aboutSelf"
        static let image = "image"
    }
    
    required convenience init?(coder aDecoder: NSCoder){
    
        guard let name = aDecoder.decodeObject(forKey: PropertyKeys.name) as? String else {
            return nil
        }
        guard let abouSelf = aDecoder.decodeObject(forKey: PropertyKeys.aboutSelf) as? String else {
            return nil
        }
        
        let image = aDecoder.decodeObject(forKey: PropertyKeys.image) as? UIImage
        
        self.init(name: name, aboutSelf: abouSelf,image: image)
    }
    

    convenience override init(){
        self.init(name: "man", online: false, image: .blue)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKeys.name)
        aCoder.encode(aboutSelf, forKey: PropertyKeys.aboutSelf)
        aCoder.encode(image, forKey: PropertyKeys.image)
    }
    
    func getJosn() -> [String: Any?]{
        if let image = imageI{
            let imageData:Data = UIImagePNGRepresentation(image)!
            let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
            return ["name": self.name,"aboutSelf":self.aboutSelf, "image": strBase64]
        }
        return ["name": self.name,"aboutSelf":self.aboutSelf, "image": nil]
    }
    
    convenience init?(json: [String:Any?]){
        guard let name = json["name"] as? String else{
            return nil
        }
        guard let aboutSelf = json["aboutSelf"] as? String else{
            return nil
        }
        var image: UIImage = #imageLiteral(resourceName: "placeholder-user")
        if let strBase64 = json["image"] as? String {
            let dataDecoded : Data = Data(base64Encoded: strBase64, options: .ignoreUnknownCharacters)!
            image = UIImage(data: dataDecoded)!
        }

        self.init(name: name, aboutSelf: aboutSelf, image: image)
    }
    
    init(name: String, aboutSelf: String, online:Bool, image: UIColor){
        self.name = name
        self.aboutSelf = aboutSelf
        self.online = online
        self.image = image
    }

    convenience init(name: String, aboutSelf: String, uri: URL?){
        self.init(name: name, aboutSelf: aboutSelf, image: nil)
        self.uri = uri
    }
    
    init(name: String, aboutSelf: String, image: UIImage?){
        self.name = name
        self.aboutSelf = aboutSelf
        self.imageI = image
    }
    
    convenience init(name: String,  online:Bool, image: UIColor){
        self.init(name: name, aboutSelf: "", online: online, image: image)
    }

}

