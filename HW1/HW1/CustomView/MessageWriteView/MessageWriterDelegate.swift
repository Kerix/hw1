//
//  MessageWriterDelegate.swift
//  HW1
//
//  Created by Виталя on 05.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

protocol MessageWriterDelegate:class {
    func sendButtonClick(text: String)
}
