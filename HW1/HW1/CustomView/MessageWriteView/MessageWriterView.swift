//
//  MessageWriterView.swift
//  HW1
//
//  Created by Виталя on 05.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit

@IBDesignable class MessageWriterView: UIView{

    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var textEditor: UITextField!
    
    weak var sender: MessageWriterDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setting()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setting()
    }
    
    private func setting(){
        let bundle = Bundle(for: MessageWriterView.self)
        bundle.loadNibNamed("MessageWriterView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    

    @IBAction func sendButtonClick(_ sender: UIButton) {
        self.sender?.sendButtonClick(text: textEditor.text!)
        textEditor.text = nil
    }
    
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }

    
}


