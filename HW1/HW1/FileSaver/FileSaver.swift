//
//  FileSaver.swift
//  HW1
//
//  Created by Виталя on 21.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

class FileSaver{
    
    static func saveDataToFile(file: [String:Any?]? , path: URL) -> Bool{
        guard let file = file else{
            print("file is nil")
            return false
        }
        do{
            let json = try JSONSerialization.data(withJSONObject: file, options: .prettyPrinted)
            try json.write(to: path, options: .atomicWrite)
        }catch{
            print("Something wrong!");
            return false
        }
        return true
    }
    
    
    static func readDataFromFile(path: URL) -> [String: Any?]? {
        do {
            let data = try Data(contentsOf: path, options: .alwaysMapped)
            let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            return dictionary
        }catch {
            print("Something wrong!");
            return nil
        }
    }
    
}
