//
// Created by Виталя on 27.10.2018.
// Copyright (c) 2018 Dreams of Pines. All rights reserved.
//

import Foundation
import MultipeerConnectivity


class NetworkApi: NSObject, ICommunicationService{

    static var reference: NetworkApi?
    weak var delegate: CommunicationServiceDelegate?
    var online: Bool = false {
        willSet{
            if online == newValue {
                return
            }
            if newValue {
                advertise.startAdvertisingPeer()
                browser.startBrowsingForPeers()
            }else{
                advertise.stopAdvertisingPeer()
                browser.stopBrowsingForPeers()
            }
        }
    }

    private var session: MCSession
    private var advertise: MCNearbyServiceAdvertiser
    private var browser: MCNearbyServiceBrowser
    private let serviceType = "tinkoff-chat"
    let mainPeer: Peer = Peer(identifier: "ker1x", name: "Vitaliy") // Временно храню его здесь, затем перемещю в другой класс

    var peerIdList: [Peer: MCPeerID] = [:]
    
    var peersList: [Peer: (connect: Bool,
                           online: Bool,
                           messages: [(peer: Peer, message: Message)])] = [:]

    func send(_ message: Message, to peer: Peer) {
        peersList[peer]?.messages.append((peer: mainPeer, message: message))
        do{
            try session.send(message.toJSONData()!, toPeers: [peerIdList[peer]!], with: MCSessionSendDataMode.reliable)
        }catch{}
    }
    
    func sendInvite(_ peer: Peer){
        browser.invitePeer(peerIdList[peer]!, to: session, withContext: nil, timeout: 60.0)
    }

    static func create()->NetworkApi{
        if let ref =  reference {
            return ref
        }
        reference = NetworkApi()
        return reference!
    }

    private override init(){
        let peerID = MCPeerID(displayName: mainPeer.identifier)
        advertise = MCNearbyServiceAdvertiser(peer: peerID, discoveryInfo: ["userName": mainPeer.name],
                serviceType: serviceType)
        browser = MCNearbyServiceBrowser(peer: peerID, serviceType: serviceType)
        session = MCSession(peer: peerID)
        super.init()

        advertise.delegate = self
        browser.delegate = self
        session.delegate = self

    }

    private func findValueById(id: MCPeerID)->Peer{
        for key in peerIdList.keys{
            if peerIdList[key] == id{
                return key
            }
        }
        return Peer(identifier: id.displayName, name: "")
    }
    
}

extension NetworkApi: MCSessionDelegate{
    public func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {

    }

    public func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    }

    public func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    }

    public func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        let peer = Peer(identifier: peerID.displayName, name: "")
        let accept = state == MCSessionState.connected || state == MCSessionState.connecting
        peersList[peer]?.connect = accept
        delegate?.communicationService(self, peer: peer, connect: accept)
    }

    public func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        if let message = Message(from: data) {
            let peer = findValueById(id: peerID)
            peersList[peer]?.messages.append((peer: peer, message: message))
            delegate?.communicationService(self, didReceiveMessage: message, from: peer)
        }
    }
    
}

extension NetworkApi: MCNearbyServiceAdvertiserDelegate{
    public func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
        delegate?.communicationService(self, didNotStartAdvertisingForPeers: error)
    }

    public func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        let peer = findValueById(id: peerID)
        delegate?.communicationService(self, didReceiveInviteFromPeer: peer){ [weak self] accept in
            self?.peersList[peer]?.connect = accept
            invitationHandler(accept, self?.session)
        }
    }
}

extension NetworkApi: MCNearbyServiceBrowserDelegate{
    public func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
        delegate?.communicationService(self, didNotStartBrowsingForPeers: error)
    }

    public func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String: String]?) {
        if let name = info?["userName"] {
            let peer = Peer(identifier: peerID.displayName, name: name)
            if peersList[peer] == nil{
                peersList[peer] = (connect: false, online: true, messages:[])
            }else{
                peersList[peer]!.online = true
            }
            peerIdList[peer] = peerID
            delegate?.communicationService(self, didFoundPeer: peer)
        }
    }

    public func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
        let peer = Peer(identifier: peerID.displayName, name: "")
        peerIdList[peer] = nil
        peersList[peer]?.online = false
        delegate?.communicationService(self, didLostPeer: peer)
    }
}

