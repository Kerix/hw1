//
// Created by Виталя on 27.10.2018.
// Copyright (c) 2018 Dreams of Pines. All rights reserved.
//

import Foundation

protocol CommunicationServiceDelegate: class {

    /// Browsing
    func communicationService(_ communicationService: ICommunicationService, didFoundPeer peer: Peer)

    func communicationService(_ communicationService: ICommunicationService, didLostPeer peer: Peer)

    func communicationService(_ communicationService: ICommunicationService, didNotStartBrowsingForPeers error: Error)

    /// Advertising
    func communicationService(_ communicationService: ICommunicationService, didReceiveInviteFromPeer peer: Peer,
                              invintationClosure: @escaping (Bool) -> Void)
    
    func communicationService(_ communicationService: ICommunicationService,
                              didNotStartAdvertisingForPeers error: Error)
    
    func communicationService(_ communicationService:ICommunicationService, peer: Peer, connect: Bool)
    
    
    /// Messages
    func communicationService(_ communicationService: ICommunicationService, didReceiveMessage message: Message,
                              from peer: Peer)

}
