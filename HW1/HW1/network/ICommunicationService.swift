//
// Created by Виталя on 27.10.2018.
// Copyright (c) 2018 Dreams of Pines. All rights reserved.
//

import Foundation

protocol ICommunicationService {
    var delegate: CommunicationServiceDelegate? { get set }
    var online: Bool { get set }
    func send(_ message: Message, to peer: Peer)
}
