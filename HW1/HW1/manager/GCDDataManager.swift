//
//  GCDDataManager.swift
//  HW1
//
//  Created by Виталя on 20.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

class GCDDataManager: DataManager{
    
    private let saveQueue = DispatchQueue(label: "com.hw1.manager.save.serial", attributes: .concurrent)

    
   func save(user: User, handler: @escaping (Bool)->()){
        saveQueue.async {
            let isSuccessfulSave = FileSaver.saveDataToFile(file: user.getJosn(), path: User.ArchiveURL)
            handler(isSuccessfulSave)
        }
    }
    
    func load(handler: @escaping (User?)->()){
        saveQueue.async {
            var user: User? = nil
            if let json = FileSaver.readDataFromFile(path: User.ArchiveURL){
                user = User(json: json)
            }
            handler(user)
        }
    }
    
}
