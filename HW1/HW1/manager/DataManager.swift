//
//  SaveDataDelegate.swift
//  HW1
//
//  Created by Виталя on 20.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

protocol DataManager {
    func save(user: User, handler: @escaping (Bool)->())
    func load(handler: @escaping (User?)->())
}
