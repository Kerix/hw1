//
//  LoadOperation.swift
//  HW1
//
//  Created by Виталя on 20.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit

class LoadOperation: Operation {

    var user: User?
    
    override func main() {
        user = nil
        if let json = FileSaver.readDataFromFile(path: User.ArchiveURL){
            user = User(json: json)
        }
    }
    
}
