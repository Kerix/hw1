//
//  SaveOperation.swift
//  HW1
//
//  Created by Виталя on 20.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import UIKit

class SaveOperation: Operation {

    var user: User?
    var success: Bool = false
    
    override func main() {
        if(isCancelled) {return}
        success = FileSaver.saveDataToFile(file: user?.getJosn(), path: User.ArchiveURL)
    }
 
}
