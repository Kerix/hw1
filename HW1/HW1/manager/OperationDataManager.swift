//
//  OperationDataManager.swift
//  HW1
//
//  Created by Виталя on 20.10.2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//

import Foundation

class OperationDataManager: DataManager{
    
    private let operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    func save(user: User, handler: @escaping (Bool) -> ()) {
        let saveOperation = SaveOperation()
        saveOperation.user = user
        saveOperation.completionBlock = { handler(saveOperation.success) }
        operationQueue.addOperation(saveOperation)
    }
    
    func load(handler: @escaping (User?) -> ()) {
        let loadOperation = LoadOperation()
        loadOperation.completionBlock = { handler(loadOperation.user)}
        operationQueue.addOperation(loadOperation)
    }
}
