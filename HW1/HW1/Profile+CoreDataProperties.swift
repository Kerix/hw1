//
//  Profile+CoreDataProperties.swift
//  HW1
//
//  Created by Виталя on 03/11/2018.
//  Copyright © 2018 Dreams of Pines. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var name: String?
    @NSManaged public var abouSelf: String?
    @NSManaged public var imageUri: URL?

}
