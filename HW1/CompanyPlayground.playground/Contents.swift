import Foundation

protocol Worker{
    var name: String {get set}
    var salary: Int {get set}
    func sendMessage(to: Worker, message: String)
    func getMessage(from: Worker, message: String)
    func tellAboutSelf()
}

enum position: String{
    case CEO
    case Developer
    case ProductManager
}

class Random{
    
    private static let messages: [String] = ["Где комменты к твоему коду?", "Ты говнокодер!", "Кто съел все печенья??!!", "Может по кофе?", "Что значит ты не можешь взломать аккааунт в вк, ты же программист", "Я начинаю циклы с 1", "Господи, убейте меня"]
    
    private static let toManagerMessages: [String] = ["Продукт-менеджер, дай ТЗ", "Продукт-менеджер, дай мне новую задачу", "Продукт-менеджер, я что-то нажал(а) и все сломалось, что делать?"]
    
    static func getDeveloperMessage()->String{
        return messages[randomInt(count: messages.count)]
    }
    
    static func getToManagerMessage()->String{
        return toManagerMessages[randomInt(count: toManagerMessages.count)]
    }
    
    static func randomInt(count: Int)->Int{
        return Int(arc4random_uniform(UInt32(count)))
    }
}


class CEO: Worker{
    var name: String
    var salary: Int
    weak var projectManager: ProjectManager?
    
    lazy var printCompany = { [weak self] in
        self?.projectManager?.printCompany()
    }
    
    lazy var printManager = { [weak self] in
        print("Продукт менеджер нашей компании: ")
        self?.projectManager?.tellAboutSelf()
        print()
    }
    
    lazy var printDevelopers = {[weak self] in
        print("Разработчики нашей компании: ")
        for dev in (self?.projectManager?.developers)! {
            dev.tellAboutSelf()
        }
        print()
    }
    
    init(name: String, salary: Int) {
        self.name = name
        self.salary = salary
    }
    
    func sendMessage(to: Worker, message: String) {
        print("CEO \(name) отправил письмо \(to.name): \(message)")
        to.getMessage(from: self, message: message)
    }
    
    func getMessage(from: Worker, message: String) {
        print("CEO \(name) получил письмо от \(from.name): \(message)")
    }
    
    func tellAboutSelf() {
        print("CEO нашей компании: ")
        print("Я, \(name), CEO нашей компании с зарплатой \(salary)\n")
    }
    
    deinit {
        print("CEO уволен!")
    }
}

class ProjectManager: Worker{
    var name: String
    var salary: Int
    weak var ceo: CEO?
    var developers: [Developer]?
    
    init(name:String, salary: Int) {
        self.name = name
        self.salary = salary
    }
    
    func printCompany(){
        print("---------------------------------------")
        print("Наша компания состоит: \n")
        ceo?.tellAboutSelf()
        ceo?.printManager()
        ceo?.printDevelopers()
        print("---------------------------------------")
    }
    
    func sendMessage(to: Worker, message: String) {
        print("Project-Manager \(name) отправил письмо \(to.name): \(message)")
        to.getMessage(from: self, message: message)
    }
    
    func getMessage(from: Worker, message: String) {
        print("ProjectManager \(name) получил письмо от \(from.name): \(message)")
    }
    
    func findAnotherDeveloper(from: Developer)->Developer{
        var developer: Developer
        repeat{
            developer = developers![Random.randomInt(count: developers!.count)]
        }while(developer === from)
        return developer
    }

    func tellAboutSelf() {
        print("Я, \(name), project-manager нашей компании с зарплатой \(salary)")
    }

    deinit {
        print("Project-Manager уволен!")
    }

}

class Developer: Worker{
    var name: String
    var salary: Int
    weak var porjectManager: ProjectManager?
    
    init(name:String, salary: Int) {
        self.name = name
        self.salary = salary
    }
    
    func tellAboutSelf() {
        print("Я, \(name), developer нашей компании с зарплатой \(salary)")
    }

    func getMessage(from: Worker, message: String){
        print("Developer \(name) получил письмо от \(from.name): \(message)")
    }
    
    func sendMessage(to: Worker, message: String){
        print("Developer \(name) отправил письмо \(to.name): \(message)")
        to.getMessage(from: self, message: message)
    }

    deinit {
        print("Developer \(name) уволен!")
    }
}

class Company{
    var ceo: CEO?
    var projectManager: ProjectManager?
    var developers:[Developer]?

    static func createCompany()->Company{
        let company = Company()
        
        company.developers = createDevelopers()
        company.projectManager = ProjectManager(name: "Gena", salary: 100000)
        company.projectManager?.developers = company.developers
        company.developers?.forEach({$0.porjectManager = company.projectManager})
        company.ceo = CEO(name: "ROCKFELLER", salary: 999999999)
        company.ceo?.projectManager = company.projectManager
        company.projectManager?.ceo = company.ceo
        
        return company
    }
    
    private static func createDevelopers()->[Developer]{
        var developers:[Developer] = []
        developers.append(Developer(name: "Jon", salary: 22222))
        developers.append(Developer(name: "Lol", salary: 221122))
        developers.append(Developer(name: "Kek", salary: 22332))
        return developers
    }
    
    func printManager(){
        ceo?.printManager()
    }
    
    func printAllCompany(){
        ceo?.printCompany()
    }
    
    func printDevelopers(){
        ceo?.printDevelopers()
    }
    
    func createDialogs(){
        createDialogWithDeveloper(to: .Developer)
        createDialogWithDeveloper(to: .ProductManager)
        createDialogWithDeveloper(to: .CEO)
    }
    
    private func createDialogWithDeveloper(to: position){
        for _ in 0...Random.randomInt(count: 7){
            let developer = developers![Random.randomInt(count: developers!.count)]
            switch to {
            case .Developer:
                developer.sendMessage(to: developer.porjectManager!.findAnotherDeveloper(from: developer), message: Random.getDeveloperMessage())
            case .ProductManager:
                developer.sendMessage(to: developer.porjectManager!, message: Random.getToManagerMessage())
            case .CEO:
                developer.sendMessage(to: developer.porjectManager!.ceo!, message: "CEO, хочу больше зарплату!!!!")
            }
        }
        print()
    }
    
    deinit {
        print("Компания обанкротилась!")
    }
    
}

func main(){
    let company = Company.createCompany()
    company.ceo?.printCompany()
    company.printAllCompany()
    company.printManager()
    company.printDevelopers()
    company.createDialogs()
}

main()






